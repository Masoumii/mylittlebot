let possibleWhoAmIResponses = [
    'Weet ik zelf ook niet zo goed...',
    'Sommige mensen noemen mij bot. Ik vind mijzelf best netjes.',
    'Bot... James Bot'
];
let possibleGreetingsResponses = [
    'Hoi!',
    'Dag!',
    'hallo... Wat kan ik voor je doen?',
    'Aloa.',
    'Bonjour!',
    'Ciao.'
];
let possiblehowAreYouResponses = [
    'Ik mag niet klagen. ',
    'Tot nu toe gaat alles goed.',
    'Het kan altijd beter.',
    'Toppie'
];
let possibleGoodbyeResponses = [
    "<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp;Je bent uitgelogd. Tot ziens!</span>",
    "<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp;Geniet van je dag, Je bent uitgelogd!</span>",
    "<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp;Bye bye, Ik heb je uitgelogd</span>",
    "<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp;Tot ziens. Uitloggen is gelukt!</span>",
    "<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp;Ok. Pas je op jezelf? Ik heb je ondertussen alvast uitgelogd!</span>"
];
let possibleWhyResponses = [
    "Daarom...",
    "Ik heb geen idee...",
    "Niet zoveel vragen stellen.",
    "Ik weet ook niet alles he...",
    "Ik heb daar nu geen antwoord op.",
    "Vraag het aan Google.",
    "Je bent wel nieuwsgierig, he?"
];
let possibleLogInResponses =[
    "Je moet ingelogd zijn om dat te kunnen doen.<br>Als wie mag ik je inloggen?",
    "Niet iedereen mag dat doen. Kun je jezelf identificeren?",
    "Voor de actie is een login vereist. Zou je je eerst in kunnen loggen?",
    "Ik weet nog niet met wie ik spreek. Log je eerst even in."
];
let possibleGratitudeResponses = [
    "Graag gedaan!",
    "Geen dank!",
    "Geen probleem!",
    "Ik doe het met plezier!"
];
let commandoResponses = {
    zaklamp_aan:"Ok. Ik heb de zaklamp aangezet.",
    zaklamp_uit:"Ok. Ik heb de zaklamp uitgezet.",
    zaklamp_fail:" Het is mij niet gelukt om de zaklamp aan of uit te zetten..."
};