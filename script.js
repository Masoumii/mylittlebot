/* jQuery */
$(function() {
    
    /* Create localStorage current time key/pair values on-load */
    let d = new Date();
    let h = (d.getHours() < 10 ? '0' : '') + d.getHours();
    let m = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
    let currTime = `${h}:${m}`;
    localStorage.setItem('last_visit_time',currTime); 
    
    if (localStorage.getItem("flashlight") === null) {
        localStorage.setItem("flashlight", "false");
    }
    
    /* Conversation Array */
    let conversationArray = [];
    /* Main URL to send commands to */
    let mainCommandUrl = "https://autoremotejoaomgcd.appspot.com/sendmessage?key=APA91bHFGSzgh_2f-1m57MAmzcLy3R19p1Uj1bPpe90Jn3pmCkPeqemMVSnpEQ1DhKc2gKvWY5rh6pz2v-1zCFKNpNIu5miGp6YJmMU_Src23y7OHeBdr-mZ4MIycXjds3Ur-GWuvogF&message=";

    /* Function to get the phone location */
    function getPhoneLocation(){
    
    let getLocation = $.getJSON("https://mijnprojecten.ml/phone-dashboard/read.php", function() {});
       
        // If getting location succeeded 
        getLocation.done(function(data) {
            let phoneLocation = "https://maps.google.com/maps?q=" + data.location + "&hl=nl;z=14&amp;output=embed";
            localStorage.setItem('phoneLocation', phoneLocation);
        });
        // If getting location failed
        getLocation.fail(function() {
            console.error("Failed to get phone location");
        });  
    }
    /* Run function on page load */
    getPhoneLocation();
    
    /* Array with conversation */
    function addToConversationList(conversation){
        let stripedHtml = $("<div>").html(conversation).text();
        conversationArray.push(stripedHtml);
        localStorage.setItem('conversationList', JSON.stringify(conversationArray));
    }
    
    /* Scroll down window */
    function scrollDown(pixels){
    $(".chat-screen").animate({
        scrollTop: '+='+pixels+'px'
        }, "slow");
    };
    
    let possibleRandomResponses = [];
    /* Get random responses function */
    let possibleResponsesRequest = $.getJSON("answers.php", function(data){});
        
        /* If requesting random responses succeeded */
        possibleResponsesRequest.done(function(data) {
            possibleRandomResponses = data;
        });
        possibleResponsesRequest.fail(function() {});
        
    /* Element variables */
    let chatInput = $("#chat-input");
    let chatScreen = $(".chat-screen");
    let sendChatBtn = $("#send-chat");

    /* String variables */
    let chatString = null;
    let respondString = null;

    /* Send chat */
    function sendChat(){

        // If chat input is not empty
        if ($("#chat-input").val() !== "") {

            /* Calculate current date when sending message */
            let d = new Date();
            let h = (d.getHours() < 10 ? '0' : '') + d.getHours();
            let m = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
            let currTime = `${h}:${m}`;

            chatString = `<div class='chat-line'>
            <img class='user-placeholder' src='img/placeholder.png'>&nbsp;&nbsp;
            ${$("#chat-input").val()} - (${currTime}) </div>`;
            $(chatScreen).append(chatString);
            $("#chat-input").blur(); // Hide keyboard on smartphones
            scrollDown(125);
            let playSound = new Audio('send_chat.mp3');
            playSound.play();
            $(this).hide().fadeIn();
            respond();
            addToConversationList($("#chat-input").val());
            $(chatInput).val("");
            return false;
        }
    }

    /* Respond chat */
    function respond() {
        
        /* To lower-case the user input */
        let userInput = $("#chat-input").val().toLowerCase();

        /* Get current time for time of chat */
        let d = new Date();
        let h = (d.getHours() < 10 ? '0' : '') + d.getHours();
        let m = (d.getMinutes() < 10 ? '0' : '') + d.getMinutes();
        let currTime = `${h}:${m}`;

        /* Response to : Random response */
        let randomAnswer = possibleRandomResponses[Math.floor(Math.random() * possibleRandomResponses.length)];

        /* Response to: Hallo  */
        if (userInput.includes("hoi") ||
            userInput.includes("hallo") ||
            userInput.includes("hi") ||
            userInput.includes("ola") ||
            userInput.includes("hola") ||
            userInput.includes("yo") ||
            userInput.includes("hay") ||
            userInput.includes("aloa") ||
            userInput.includes("bonjourno") ||
            userInput.includes("salam") ||
            userInput.includes("hay") ||
            userInput.includes("hai") ||
            userInput.includes("hello") ||
            userInput.includes("ewa") ||
            userInput.includes("iwa") ||
            userInput.includes("heej") ||
            userInput.includes("hej") ||
            userInput.includes("hey")
        ) {
            randomAnswer = possibleGreetingsResponses[Math.floor(Math.random() * possibleGreetingsResponses.length)];
        }

        /* Response to: Waarom  */
        if (userInput.includes("waarom") ||
            userInput.includes("hoe komt het dat") ||
            userInput.includes("hoe komt dat")||
            userInput.includes("hoezo")||
            userInput.includes("wanneer")||
            userInput.includes("waar kan")||
            userInput.includes("waar is")||
            userInput.includes("waar blijft")||
            userInput.includes("wat ")) {
            randomAnswer = possibleWhyResponses[Math.floor(Math.random() * possibleWhyResponses.length)];
        }

        /* Response to: Hoe gaat het  */
        if (userInput.includes("hoe gaat het") ||
            userInput.includes("hoe is het") ||
            userInput.includes("alles goed")) {
            randomAnswer = possiblehowAreYouResponses[Math.floor(Math.random() * possiblehowAreYouResponses.length)];
        }

        /* Response to: Doei  */
        if (userInput.includes("doei") ||
            userInput.includes("dag") ||
            userInput.includes("ciao") ||
            userInput.includes("later") ||
            userInput.includes("laters") ||
            userInput.includes("tot kijk") ||
            userInput.includes("tot kijks") ||
            userInput.includes("doeg")
        ) {
            randomAnswer = possibleGoodbyeResponses[Math.floor(Math.random() * possibleGoodbyeResponses.length)];
            localStorage.setItem('isAdmin', 'false');
            localStorage.setItem('username', 'guest'); // empty username variable 
        }

        /* Response to: Wie ben jij ? */
        if (userInput.includes("bedankt") ||
            userInput.includes("dankje") ||
            userInput.includes("dankjewel") ||
            userInput.includes("dank u wel")) {
            randomAnswer = possibleGratitudeResponses[Math.floor(Math.random() * possibleGratitudeResponses.length)];
        }

        /* Response to: Wie ben jij ? */
        if (userInput.includes("wie ben") ||
            userInput.includes("hoe heet jij") ||
            userInput.includes("hoe heet je") ||
            userInput.includes("wat is je naam")) {
            randomAnswer = possibleWhoAmIResponses[Math.floor(Math.random() * possibleWhoAmIResponses.length)];
        }

        /* Response to: Log in ? */
        if (userInput.includes("masoumi")) {
            randomAnswer = "<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp;Welkom, Masoumi. Je bent ingelogd!";
            localStorage.setItem('isAdmin', 'true'); // Set isAdmin localStorage 
            localStorage.setItem('username', 'masoumi'); // Set isUsername localStorage
            localStorage.setItem('last_login_time', currTime);
        }

        if (userInput.includes("log mij in") && !userInput.includes("masoumi")) {
            randomAnswer = "<span class='errorLine'><img width='20' src='img/error.png'>&nbsp;&nbsp;Helaas. Ik heb je niet kunnen herkennen Log eerst even in.";
        }

        /* Response to: Log out? */
        if (userInput.includes("log") && userInput.includes("uit") ||
            userInput.includes("uitloggen")) {
            randomAnswer = "<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp;Ok, Je bent weer uitgelogd!";
            localStorage.setItem('isAdmin', 'false'); // Set localStorage admin to false
            localStorage.setItem('username', ''); // empty username variable 
        }

        /* Commando: Locatie */
        if (userInput.includes("locatie") || userInput.includes("waar is mijn telefoon")) {

            if (localStorage.getItem('isAdmin') === 'true') {

                let location = `${localStorage.getItem('phoneLocation')}&hl=nl;z=14&output=embed`; // Get localStorage 
                randomAnswer = `<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp; Je telefoon bevindt zich momenteel hier:</span><br><br><br><hr>
                <iframe frameborder="0" style="width:95%" height="200" src='${location}'></iframe><br>`;

            } else {
                randomAnswer = `<span class='errorLine'><img width='20' src='img/error.png'>&nbsp;&nbsp;${possibleLogInResponses[Math.floor(Math.random() * possibleLogInResponses.length)]}</span>`;
            }
        }
        
           /* Commando: Locatie */
        if (userInput.includes("locatie") || userInput.includes("waar is mijn telefoon")) {

            if (localStorage.getItem('isAdmin') === 'true') {

                let location = `${localStorage.getItem('phoneLocation')}&hl=nl;z=14&output=embed`; // Get localStorage 
                randomAnswer = `<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp; Je telefoon bevindt zich momenteel hier:</span><br><br><br><hr>
                <iframe frameborder="0" style="width:95%" height="200" src='${location}'></iframe><br>`;

            } else {
                randomAnswer = `<span class='errorLine'><img width='20' src='img/error.png'>&nbsp;&nbsp;${possibleLogInResponses[Math.floor(Math.random() * possibleLogInResponses.length)]}</span>`;
            }
        }

        /* Commando : Flashlight on  */
        if (userInput.includes("zaklamp") && userInput.includes("aan") ||
            userInput.includes("zaklamp") && userInput.includes("aanzetten")) {

            /* If the user is admin */
            if (localStorage.getItem('isAdmin') === 'true') {

                /* If the flashlight is turned off */
                if (localStorage.getItem('flashlight') === 'false') {

                    /* Command to toggle flashlight */
                    let commandFlashLight = $.get(`${mainCommandUrl}cmd_toggle_flashlight`, function() {});

                    /* If command run succesful */
                    commandFlashLight.done(function() {
                        randomAnswer = `<span style='color:#3fdc94'>
                    <img width='20' src='img/ok.png'>&nbsp;&nbsp;
                    ${commandoResponses.zaklamp_aan}</span>`;
                    
                        localStorage.setItem('flashlight', 'true');
                        
                    });
                    /* If command run failed */
                    commandFlashLight.fail(function() {
                        randomAnswer = `<span style='color:#ff857c'>
                    <img width='20' src='img/error.png'>&nbsp;&nbsp;
                   ${commandoResponses.zaklamp_fail}</span>`;
                    });
                    /* If the flashlight is already turned on*/
                } else {
                    randomAnswer = `De zaklamp staat al aan. Ik kan hem wel voor je uit zetten.`;
                }
            } else {
                /* Ask the user to log in before being able to send commands */
                randomAnswer = `<span style='color:#ff857c'>
                <img width='20' src='img/error.png'>&nbsp;&nbsp;
                ${possibleLogInResponses[Math.floor(Math.random() * possibleLogInResponses.length)]}</span>`;
            }
        }

        /* Commando : Flashlight off  */
        if (userInput.includes("zaklamp") && userInput.includes("uit")) {

            /* If user recognized as Admin */
            if (localStorage.getItem('isAdmin') === 'true') {

                /* Set flashlight variable in localStorage */
                if (localStorage.getItem('flashlight') === 'true') {

                    /* Send command to turn on the flashlight */
                    let commandFlashLight = $.get(`${mainCommandUrl}cmd_toggle_flashlight`, function() {});

                    /* If sending succeeded */
                    commandFlashLight.done(function() {
                        randomAnswer = `<span class='okLine'><img width='20' src='img/ok.png'>&nbsp;&nbsp;${commandoResponses.zaklamp_uit}</span>`;
                    });
                    /* If sending command failed */
                    commandFlashLight.fail(function() {
                        randomAnswer = `<span class='errorLine'><img width='20' src='img/error.png'>&nbsp;&nbsp;${zaklamp_fail}:</span>`;
                    });

                    /* Set localStorage: flashlight variable FALSE */
                    localStorage.setItem('flashlight', 'false');

                }
                /* If flashlight already turned off */
                else {
                    randomAnswer = `De zaklamp staat al uit. Ik kan hem wel voor je <a class='turn-on-flashlight'>aan zetten</a> als jij dat wilt.`;
                }
            }
            /* If not logged in / not recognized as admin */
            else {
                randomAnswer = "<span class='errorLine'><img width='20' src='img/error.png'>&nbsp;&nbsp;Het is mij niet gelukt om de zaklamp uit te zetten...<br> Jij bent niet herkend als mijn meester.</span>";
            }
        }

        /* Determine response time based on the length of the response */
        let responseTime = Math.round((randomAnswer.length * 0.5) * 1000);
        if (responseTime > 6 * 1000) {
            responseTime = 6 * 1000;
        }

        /* Log how much time is needed to respond to user input */
        console.log(`Required time to respond to: ${responseTime} seconds`);

        /* Set timeout function to show writing status */
        setTimeout(function() {
            respondString = $(`<div class='chat-line loading'>
            <img class='bot-placeholder' src='img/ai_placeholder.gif'>&nbsp;&nbsp;
            <img src='img/writing.gif' width='35'>
            <span class='typing'>aan het typen...</span></div><br>`);
            
            /* Append / create the chat line */
            $(chatScreen).append(respondString);
            
            /* Scroll down x pixels */
            scrollDown(125);

            /* Timeout function to Respond / append to chat */
            setTimeout(function(){
            $(".loading").remove();
                respondString = `<div class='chat-line'>
                <img class='bot-placeholder' src='img/ai_placeholder.gif'>&nbsp;&nbsp;
                ${randomAnswer} - (${currTime}) </div>
                <br>`;
                $(chatScreen).append(respondString);

                let playSound = new Audio('send_chat.mp3');
                playSound.play();
                addToConversationList(randomAnswer);
            }, responseTime);
            
        }, 3000);
    }

    /* Pressing the send button */
    $(sendChatBtn).on("click", function() {
        sendChat();
    });
    /* Pressing Enter */
    $(document).keypress(function(e) {
        if (e.which === 13) {
            sendChat();
        }
    });

});