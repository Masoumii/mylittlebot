#MyLittleBot#

![alt text](https://mylittlebot.ml/img/ai_placeholder_small.gif)

This little bot can perform a range of actions including:

---

###Current Features###

- Having **simple conversations**
- **Logging** users **in** and **out**
- **Control** your **smartphone** with commands/conversation

---

###Upcoming Features###

- Fetch **weather details**, for example: *"How's the weather in [city]?"*
- Fetch **news**, for example: *"Show me some news about [topic]"* or *"Show me the latest news"*
- Sending **SMS**, for example: *"Send a SMS to [contact] with [SMS content]"*
- Making **calls**, for example: *"Call [contact]"*
- **Making appointments** in **calendar**, for example *"Make an appointment for [topic] on [date]"*

---

### Usage ###

- Simply ask the bot something like *"Hello"* or *"How are you?"* and he will respond
- Log in as user/admin: *"Log me in with [username]"*
- Log in as user/admin and perform command in one line: *"It's me, [username]. Now show me phone's location"*
- Log out: *"Log me out"*